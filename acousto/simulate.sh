#!/bin/sh

echo Generate Mesh
./generate_mesh.py "$1"

echo Cleaning CRLF
./dos2unix.py sim/\*.msh

echo Running simulation...
sudo docker run -it --rm --mount type=bind,source="$(pwd)",target=/work --init uiemma/acousto:v16debian /work/run.sh "$@"

for i in sim/*/; 
do mkdir -p outputs/$(basename -- "$i");
   mv $i* outputs/$(basename -- "$i")/; 
   rmdir $i; 
done
#!/usr/bin/env python3

import sys
import os
import re
import glob
import shutil
import matplotlib.pyplot as plt
import csv
import numpy as np
import scipy.signal
import math
import json

geo_template = 'instrument.template'
geo_file = 'instrument.opt.geo'
geo_out = geo_file.replace('.geo', '.msh')

simconfig_template = 'instrument-optimize.template'
simconfig_file = 'instrument-optimize.opt.cfg'

resultdir = 'instrument-optimize'
simname = 'instrument-optimize'

def simulate(runid, parameters, freq_start, freq_stop, freq_count=32):
	print(f'Run id: {runid}')

	print('Generate geometry')

	with open(f'models/{geo_template}') as fp:
		content = fp.read()
	for k,v in parameters.items():
		val = v if type(v) is str else f'{v:f}'
		content = content.replace(f'${k}', val)
	with open(f'models/{geo_file}', 'w') as fp:
		fp.write(content)

	print('Generate simulation config')

	with open(f'sim/{simconfig_template}') as fp:
		content = fp.read()
	content = content.replace('%freq_start%', f'{freq_start:f}')
	content = content.replace('%freq_stop%', f'{freq_stop:f}')
	content = content.replace('%freq_count%', f'{freq_count}')
	with open(f'sim/{simconfig_file}', 'w') as fp:
		fp.write(content)

	print('Run simulation')
	os.system(f'./simulate.sh {simconfig_file}')

	print('Find result')
	outputdir = glob.glob(f'outputs/{simname}-*')
	if len(outputdir) != 1:
		raise Exception('Ambiguous output folder')
	outputdir = outputdir[0]

	print('Store result')
	os.system(f'mkdir -p {resultdir}/{runid}/')
	os.system(f'mv {outputdir}/* {resultdir}/{runid}/')
	os.system(f'rmdir {outputdir}')

	runinfo = f'{resultdir}/{runid}/{simname}-runinfo.out'
	with open(runinfo, 'w') as fp:
		print(f'runid: {runid}', file=fp)
		print(f'parameters: {json.dumps(parameters)}', file=fp)
		print(f'freq_start: {freq_start}', file=fp)
		print(f'freq_stop: {freq_stop}', file=fp)
		print(f'freq_count: {freq_count}', file=fp)

	meshfile = f'{resultdir}/{runid}/{simname}-models.msh'
	os.system(f'cp sim/{geo_out} {meshfile}')

	return True

def read_result(runid):
	file_m1 = f'{resultdir}/{runid}/{simname}-fresp-mic1.out'
	file_m2 = f'{resultdir}/{runid}/{simname}-fresp-mic2.out'

	freqs = []
	mic1 = []
	mic2 = []

	with open(file_m1) as fp:
		csvr = csv.reader(fp, delimiter=' ')
		next(csvr)  # skip header
		for row in csvr:
			freqs += [float(row[2])]
			mic1 += [float(row[-3]) + 1j * float(row[-2])]

	with open(file_m2) as fp:
		csvr = csv.reader(fp, delimiter=' ')
		next(csvr)  # skip header
		for row in csvr:
			mic2 += [float(row[-3]) + 1j * float(row[-2])]

	freqs = np.array(freqs)
	mic1 = np.array(mic1)
	mic2 = np.array(mic2)

	impe = np.abs((mic1 + mic2)/2  * (2j*math.pi*freqs) / (mic1 - mic2))

	return freqs, impe

def optimize(parameters, runid_prefix, target_freq, target_precision, guess_parameter, guess_freq, iteration_count=10, freq_count=32):
	counter = 0

	parameter_change = None
	for k,v in parameters.items():
		if type(v) is str and v.startswith('search'):
			if parameter_change is not None:
				raise Exception('Can only search 1 parameter')
			if v == 'search+' or v == 'search-':
				parameter_change = k, v
	if parameter_change is None:
		raise Exception('Search parameter not found')

	def evaluate_point(pos, freq_start, freq_stop, freq_count):
		nonlocal counter
		counter += 1
		runid = f'{runid_prefix}{counter:04d}'
		print(f'[{runid}] Evaluate {parameter_change[0]}={pos}'
			  f' from {freq_start}Hz to {freq_stop}Hz in {freq_count} steps')

		current_parameters = parameters.copy()
		current_parameters[parameter_change[0]] = pos
		simulate(runid, current_parameters, freq_start, freq_stop, freq_count)

		x, y = read_result(runid)
		lowpeaks, _ = scipy.signal.find_peaks(-y)
		freqs = list(x[lowpeaks])

		print('Lower Peaks:', freqs)

		if len(freqs) < 1:
			raise Exception('No resonance found')

		error = (max_freq - min_freq) / (freq_count - 1) # uncertainty

		return freqs[0], error

	# Bisection method

	min_freq, max_freq = guess_freq
	a, b = guess_parameter

	a_freq, a_error = evaluate_point(a, min_freq, max_freq, freq_count)
	b_freq, b_error = evaluate_point(b, min_freq, max_freq, freq_count)

	for it in range(iteration_count):
		# update freq range
		min_freq, min_error = min((a_freq, a_error), (b_freq, b_error))
		max_freq, max_error = max((a_freq, a_error), (b_freq, b_error))
		min_freq = max(guess_freq[0], min_freq - min_error)
		max_freq = min(guess_freq[1], max_freq + max_error)
		if max_freq - min_freq < target_precision:
			break

		mid = (a+b)/2

		while True:
			mid_freq, mid_error = evaluate_point(mid, min_freq, max_freq, freq_count)

			if abs(mid_freq - target_freq) < mid_error:
				# uncertain => narrow down
				min_freq = max(guess_freq[0], mid_freq - mid_error)
				max_freq = min(guess_freq[1], mid_freq + mid_error)
				continue
			elif mid_freq < target_freq:
				if parameter_change[1] == 'search+':
					a, a_freq, a_error = mid, mid_freq, mid_error
				elif parameter_change[1] == 'search-':
					# need higher frequency => decrease length
					b, b_freq, b_error = mid, mid_freq, mid_error
			else:
				if parameter_change[1] == 'search+':
					b, b_freq, b_error = mid, mid_freq, mid_error
				elif parameter_change[1] == 'search-':
					# need lower frequency => increase length
					a, a_freq, a_error = mid, mid_freq, mid_error
			break

	best = (a+b)/2
	print(f'Best result: {best}')

	return best

def plot_result(target_freq, num):
	lengths, freqs = [], []
	for counter in range(1, num+1):
		runid = f'{target_freq}Hz_{counter:04d}'
		x, y = read_result(runid)
		lowpeaks, _ = scipy.signal.find_peaks(-y)
		freq = list(x[lowpeaks])[0]

		runinfo = f'{resultdir}/{runid}/{simname}-runinfo.out'
		with open(runinfo) as fp:
			for line in fp:
				if line.startswith('parameters: '):
					param = json.loads(line[len('parameters: '):])
					length = param['LENGTH']
					lengths += [length]
					freqs += [freq]
					plt.text(length, freq, f'#{counter}')

	plt.plot(lengths, freqs, 'x')
	plt.title('Length vs Frequency')
	plt.show()

if __name__ == '__main__':
	# Optimize to 440Hz with 2Hz precision from length = 0.1m to 0.5m
	# 0.1m and 0.5m should have freq within 10~1500Hz

	# search+, search- is relation of parameter to frequency
	# For example frequency = k/length in this case use search-
	target_freq = 440
	target_freq_hole = 466.16

	parameters = {
		'LENGTH': 'search-',
		'H1R': 0.01,
		'H2R': 0.04,
		'H3R': 0.0,
		'H3P': 0.35,
	};

	length = optimize(
		parameters=parameters,
		runid_prefix=f'{target_freq}Hz_phase1_',
		target_freq=target_freq, 
		target_precision=2, 
		guess_parameter=[0.1, 0.5], 
		guess_freq=[10, 1500], 
		iteration_count=25)

	parameters['LENGTH'] = length

	# Generate final result
	simulate(f'{target_freq}Hz_phase1_final', 
			parameters=parameters,
			freq_start=4,
			freq_stop=2000,
			freq_count=1024)

	# Search 2nd parameter
	parameters['H3R'] = 'search+'

	h3radius = optimize(
		parameters=parameters,
		runid_prefix=f'{target_freq}Hz_phase2_',
		target_freq=target_freq_hole, 
		target_precision=2, 
		guess_parameter=[0.0, parameters['LENGTH']/10], 
		guess_freq=[target_freq/2, 1500], 
		iteration_count=25)

	parameters['H3R'] = h3radius

	# Generate final result
	simulate(f'{target_freq}Hz_phase2_final', 
			parameters=parameters,
			freq_start=4,
			freq_stop=2000,
			freq_count=1024)
#!/usr/bin/env python3

import matplotlib.pyplot as plt
import csv
import os.path
import sys

argv = sys.argv

if __name__ == '__main__':
	if len(argv) > 1:
		d = argv[1].rstrip('/')
		simname = '-'.join(os.path.basename(d).split('-')[:-3])
		micname = argv[2] if len(argv) > 2 else 'mic1'

		file = os.path.join(d, f'{simname}-fresp-{micname}.out')

		plot_freq = []
		plot_ampl = []

		with open(file) as fp:
			csvr = csv.reader(fp, delimiter=' ')
			next(csvr)  # skip header
			for row in csvr:
				plot_freq += [float(row[2])]
				plot_ampl += [float(row[-1])]

		plt.plot(plot_freq, plot_ampl, '-x')
		plt.show()
	else:
		print('plot.py <output_folder> [micname]')
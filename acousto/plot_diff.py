#!/usr/bin/env python3

import matplotlib.pyplot as plt
import csv
import os.path
import sys
import numpy as np
import scipy.signal
import math

argv = sys.argv

mic_dx = 0.005
plot_type = 'impedance'

if __name__ == '__main__':
	if len(argv) > 1:
		if len(argv) > 2:
			plot_type = argv[2].lower()

		d = argv[1].rstrip('/\\')
		simname = '-'.join(os.path.basename(d).split('-')[:-3])
		if not simname:
			simname = os.path.dirname(d)

		file_m1 = os.path.join(d, f'{simname}-fresp-mic1.out')
		file_m2 = os.path.join(d, f'{simname}-fresp-mic2.out')

		freqs = []
		mic1 = []
		mic2 = []

		with open(file_m1) as fp:
			csvr = csv.reader(fp, delimiter=' ')
			next(csvr)  # skip header
			for row in csvr:
				freqs += [float(row[2])]
				mic1 += [float(row[-3]) + 1j * float(row[-2])]

		with open(file_m2) as fp:
			csvr = csv.reader(fp, delimiter=' ')
			next(csvr)  # skip header
			for row in csvr:
				mic2 += [float(row[-3]) + 1j * float(row[-2])]

		freqs = np.array(freqs)
		mic1 = np.array(mic1)
		mic2 = np.array(mic2)

		flow = np.abs((mic1 - mic2) / mic_dx)

		impe = np.abs(mic_dx / (mic1 - mic2) * (mic1 + mic2)/2  * (2j*math.pi*freqs))

		# handle recipro and negative
		title = ''
		modifier = lambda x: x
		if plot_type.startswith('1/'):
			plot_type = plot_type[2:]
			modifier = lambda x: 1/x
			title = '1/'
			if plot_type.startswith('-'):
				plot_type = plot_type[1:]
				modifier = lambda x: -1/x
				title = '-1/'
		elif plot_type.startswith('-'):
			plot_type = plot_type[1:]
			modifier = lambda x: -x
			title = '-'

		# set data
		if plot_type == 'pressure' or plot_type == 'p':
			select = np.abs(mic1 * (2j*math.pi*freqs))
			title += 'pressure'
		elif plot_type == 'potential' or plot_type == 'phi':
			select = np.abs(mic1)
			title += 'velocity potential'
		elif plot_type == 'flow' or plot_type == 'v' or plot_type == 'u':
			select = flow
			title += 'flow'
		elif plot_type == 'impedance' or plot_type == 'z':
			select = impe
			title += 'impedance'
		else:
			raise Exception('Bad plot_type')

		# apply
		select = modifier(select)

		peaks, _ = scipy.signal.find_peaks(select)
		peaks2, _ = scipy.signal.find_peaks(-select)

		peaks = np.concatenate((peaks, peaks2))
		peaks.sort(kind='mergesort')

		plt.plot(freqs, select, '-b')
		plt.plot(freqs[peaks], select[peaks], 'xr')

		# plt.plot((0,np.max(freqs)), (0.5,0.5), '-k')

		for i in peaks:
			plt.text(freqs[i], select[i], f'{freqs[i]:.1f} Hz')

		# plt.ylim([0,1])
		plt.yscale('log')

		plt.title(f'{simname} - {title}')
		plt.show()
	else:
		print('plot_diff.py <output_folder> [plot_type]')
		print('plot_type: pressure, potential, flow, impedance')
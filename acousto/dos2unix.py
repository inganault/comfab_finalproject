#!/usr/bin/env python3

import glob
import sys

pattern = sys.argv[1]

for filename in glob.iglob(pattern):
	with open(filename, 'rb') as open_file:
		content = open_file.read()
	content_new = content.replace(b'\r\n', b'\n')
	if content != content_new:
		with open(filename, 'wb') as open_file:
			open_file.write(content_new)
		print(f'Converted {filename}')

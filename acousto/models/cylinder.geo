// Gmsh project created on Sun Apr 26 14:40:56 2020
SetFactory("OpenCASCADE");

//+
Cylinder(1) = {0, 0, 0, 20, 0, 0, 2, 2*Pi};
//+
Cylinder(2) = {-10, 0, 0, 50, 0, 0, 1, 2*Pi};
//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Delete; }
//+
ReverseMesh Surface{3};
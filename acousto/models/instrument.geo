//+
SetFactory("OpenCASCADE");

length = 0.4;
hole1_radius = 0.5;
hole2_radius = 0.6;

Point(1) = {-length/2, 0, hole1_radius};
Point(2) = {-length/3, 0, length/6};
Point(3) = {length/6, 0, length/3};
Point(4) = {length/2, 0, hole2_radius};

Bezier(10) = {1, 2, 3, 4};
//+
Extrude {{1, 0, 0}, {0, 0, 0}, Pi/2} {
  Curve{10}; 
}
Extrude {{1, 0, 0}, {0, 0, 0}, Pi/2} {
  Curve{13}; 
}
Extrude {{1, 0, 0}, {0, 0, 0}, Pi/2} {
  Curve{16}; 
}
Extrude {{1, 0, 0}, {0, 0, 0}, Pi/2} {
  Curve{19}; 
}

//+
Curve Loop(5) = {19, 21, -22, -20};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {10, 12, -13, -11};
//+
Plane Surface(6) = {6};
//+
Curve Loop(7) = {13, 15, -16, -14};
//+
Plane Surface(7) = {7};
//+
Curve Loop(8) = {16, 18, -19, -17};
//+
Plane Surface(8) = {8};

// Move X to make sources at (0, 0, 0) and point into +X
Translate {length/2, 0, 0} {
	Surface {1, 2, 3, 4};
	Surface {5, 6, 7, 8};
}
ReverseMesh Surface {5, 6, 7, 8};

// Export this
Physical Surface("inside") = {5, 6, 7, 8};

Mesh.CharacteristicLengthFactor = 0.5;
Mesh 2;
RecombineMesh;

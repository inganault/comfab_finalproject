//+
SetFactory("OpenCASCADE");
Cylinder(1) = {0, 0, 0, 0.305, 0, 0, 0.0205, 2*Pi};
ReverseMesh Surface{1, 2, 3};
Physical Surface("cy") = {1, 2, 3};

Mesh.CharacteristicLengthFactor = 0.36;
Mesh 2;
RecombineMesh;

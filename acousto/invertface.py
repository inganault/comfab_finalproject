with open("sim/test-cylinder.msh") as fp:
	with open("sim/test-cylinder.inverted.msh", 'w', newline="\n") as fpo:
		for l in fp:
			l = l.strip('\r').split()
			if len(l) >= 5+3:
				l = l[:5] + l[5:][::-1]
			fpo.write(' '.join(l) + '\n')
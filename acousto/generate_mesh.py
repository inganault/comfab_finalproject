#!/usr/bin/env python3

import sys
import os
import re

if __name__ == '__main__':
	if len(sys.argv) <= 1:
		print('generate_mesh.py <simulation config files> [param1=value1 param2=value2 ...]')
		print('generate_mesh.py clean')
	else:
		config = sys.argv[1]
		if config.lower() == 'clean':
			os.system('rm sim/*.msh')
		else:
			# Parse config
			models = []
			with open(f'sim/{config}') as fp:
				for line in fp:
					line = line.strip()
					if line.startswith('#'):
						continue
					found = re.search(r'"([^"]+)\.msh"', line)
					if found:
						models += [found.group(1)]
			for model in models:
				template = f'models/{model}.template'
				source = f'models/{model}.geo'
				target = f'sim/{model}.msh'

				# build template
				if os.path.isfile(template):
					print(f'building {source} using {template}')
					os.system(f'rm {target}')
					os.system(f'cp {template} {source}')
					for x in sys.argv[2:]:
						k, v = x.split('=')
						os.system(f'sed \'s/${k}/{v}/g\' {source} > tmp; mv tmp {source}')

				if not os.path.isfile(source):
					raise Exception(f'{source} not found!')
				if os.path.isfile(target):
					if os.path.getmtime(source) < os.path.getmtime(target):
						print(f'{target} is already up-to-date. skiping...')
						continue
				os.system(f'gmsh "{source}" -save -format msh22 -o "{target}"')

## Generating mesh

- Make sure normals point inward.
- Use quad mesh if possible. (Gmsh -> Mesh -> Recombine)
- Remove outward pointing surfaces for better performance. (Gmsh -> Mesh -> Delete -> Surfaces)


## Simulation

- !! Microphone placement affect simulation results
- Run with `./simulation.sh test-open.cfg`
- See some result with `./plot-diff.py outputs/test-open-2020-XX-XX`